# terraform-guide

Uma forma mais simples de administrar a infraestrutura utilizando uma ferramenta bem popular no mundo DevOps. 

É Open Source e não está presa a nenhum provedor. Habilidade essencial para o profissional de nuvem. 

É possível automatizar processos complexos na nuvem de forma eficiente, economizando tempo.  

Com o Terraform é possível administrar ambientes na AWS, Google Cloud, Vmware vSpheare, não importa o provedor, é muiltiplataforma. 

Solução agnóstica, suporta vários provedores de nuvem.

Soluções agnósticas são programas, aplicativos, sistemas ou softwares que foram desenvolvidos para serem compatíveis e executados em diversos ambientes, sistemas operacionais e hardwares, sem estarem vinculados a algo especifico. 

<!-- TOC -->
- [1. Introdução](#10-introdução)
  - [1.1 O que é Terraform](#11-o-que-é-terraform)
  - [1.2 Como o Terraform funciona?](#12-como-o-terraform-funciona)
  - [1.3 Por que utilizar o Terraform?](#13-por-que-utilizar-o-terraform)
  - [1.4 Ferramentas de IaC conhecidas no mercado.](#14-ferramentas-de-iac-conhecidas-no-mercado)

- [2. Setup do ambiente de trabalho](#20-setup-do-ambiente-de-trabalho)
  - [2.1 Criar um conta na cloud desejada.](#21-criar-um-conta-na-cloud-desejada)
  - [2.2 Instalação do VSCode](#22-instalação-do-vscode)
  - [2.3 Instalação do Terraform](#23-instalação-do-terraform)
  - [2.4 Instalação do TFENV](#24-instalação-do-tfenv)
  - [2.5 Instalar e configurar o utilitário AWS CLI](#25-instalar-e-configurar-o-utilitário-aws-cli)
  - [2.6 Criar um par de chaves SSH](#26-criar-um-par-de-chaves-ssh)
  - [2.7 Criar usuário programático na AWS](#27-criar-usuário-programático-na-aws)
  - [2.8 Configurando credencias de acesso](#28-configurando-credencias-de-acesso)
  - [2.9 Terraform utilizando Docker](#29-terraform-utilizando-docker)

- [3. Guia Básico](#3-guia-básico)
  - [3.1 Documentação](#31-documentação)   
  - [3.2 Estrutura de arquivos e diretórios](#32-estrutura-de-arquivos-e-diretórios) 
  - [3.3 Estrutura de blocos da linguagem HCL](#33-estrutura-de-blocos-da-linguagem-hcl)        
  - [3.4 Configuração do bloco Terraform](#34-configuração-do-bloco-terraform)
  - [3.5 Terraform CLI](#35-terraform-cli)
  - [3.6 Credências de acesso](#36-credências-de-acesso)
  - [3.7 Criação de Bucket na AWS](#37-criação-de-bucket-na-aws) 

- [4. Variáveis](#4-variáveis)

- [5. Referenciando atributos de outros blocos](#5-referenciando-atributos-de-outros-blocos)

- [6. Local Values](#6-local-values)

- [7. Outputs](#7-outputs)
  - [7.1 Utilizando os Outputs](#71-utilizando-os-outputs)


- [8. Criar Resources](#8-criar-resources)
  - [8.1 Criar Security Group](#81-criar-security-group)
  - [8.2 Configurando VPC](#82-configurando-vpc)
  - [8.3 Configurando keypair](#83-configurando-keypair)


- [9. Configurando dependência entre os recursos ](#9-configurando-dependência-entre-os-recursos)

- [10. Novos recursos em diferentes regiões ](#10-novos-recursos-em-diferentes-regiões)

- [11. Substituindo um recurso com o comando abaixo](#11-substituindo-um-recurso-com-o-comando-abaixo)


<!-- TOC -->

## 1. Introdução

### 1.1 O que é Terraform?

- Ferramenta mais utilizada de infraestrutura como código (IaC) no mundo DevOps;
- Rápido, eficiente e replicável;
- Utiliza linguagem HCL (Hashicorp Configuration Language);
- Trabalha de forma declarativa;
- A Hashicorp disponibiliza uma extensa lista de providers oficias;

### 1.2 Como o Terraform funciona?

O núcleo do Terraform usa duas fontes de entrada:

1) Seus arquivos de configuração (seu estado desejado)
2) O estado atual, que é gerenciado pelo Terraform. 

O Terraform ler os arquivos de configuração e compara com o estado atual(tfstate) que ele tem salvo. 

Segue o fluxo de funcionamento 

1) Escrever (write): Você define os recursos que podem estar em vários provedores de nuvem. 

2) Plano (plan): É criado um plano de execução que descreve a infraestrutura que será criada. 

O Terraform abstrai as especificidades de diferentes provedores de infraestrutura, permitindo que você defina a infraestrutura como código e automatize o provisionamento em vários ambientes.


### 1.3 Por que utilizar o Terraform?

- Simplicidade da linguagem 
- Multicloud 
- Cria os recursos de forma declarativa 
- Documentação clara e bem estruturada 
- Pode ser usado em conjunto com outras ferramentas de IaC
- Idempotente - É a capacidade de ser executado várias vezes sem que o resultado se altere. 
    - Quando um comando é executado as informações de construção ficam armazenadas em um arquivo de estado (tfstate)
- Quando você usa o Terraform para fazer o deploy de sua infraestrutura ele fica mais *rápido*, *eficiente*, *replicável* e *livre de erros humanos*.


### 1.4 Ferramentas de IaC conhecidas no mercado.

- Terraform
- CloudFormation
- Ansible
- Chef
- Puppet
- Azure ARM Templates 
- Cloud Deployment Manager Google 


## 2. Setup do ambiente de trabalho

### 2.1 Criar um conta na cloud desejada.

É importante que você esteja familiarizado com a cloud que será automatizada, mandatório o conhecimento dos recursos que serão codificados. 

Por exemplo, para criarmos uma instância EC2 precisamos ter um security group criado, não é o Terraform que diz isso, que especifica isso é a AWS. 

- [ ] Criar um conta gratuita na **AWS** or **Azure** or **Google**;
        

### 2.2 Instalação do VSCode
- Instalar o **Visual Studio Code**
  - Instalar as seguintes **extensões** do Visual Studio Code
    - *Material Icon Theme*
    - **HashiCorp Terraform**
    - **HashiCorp HCL**
    - **Terraform**

### 2.3 Instalação do Terraform.

Instalar o Terraform no MacOS.

```zsh
➜  terraform-guide git:(main) brew tap hashicorp/tap
➜  terraform-guide git:(main) brew install hashicorp/tap/terraform
➜  terraform-guide git:(main) terraform -version
```


### 2.4 Instalação do TFENV

Utilizando um gerenciador de versões do Terraform(terraform version manager) facilita a manipulação de diferentes projetos sendo possível realizar o switch de versão a gosto do freguês.

```zsh
$ brew install tfenv
$ tfenv list
$ tfenv list-remote
$ tfenv install 1.6.6
$ tfenv install latest
$ tfenv use 1.6.6
```

- [ ] Se for criado na raiz do projeto um arquivo com o nome **.terraform-version** com a versão do Terraform, ele utilizará como padrão
 
- [ ] Se for criado uma variável de nome **export TFENV_TERRAFORM_VERSION=1.6.4** passando a versão ele usará como default

### 2.5 Instalar e configurar o utilitário AWS CLI

Pesquise no Google por aws cli install. 

- [AWSCLI] https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

### 2.6 Criar um par de chaves SSH

Criar um par de chaves local SSH para conexão remota com o EC2. A chave pública criada deverá ser importada na região que será provisionado os recursos. 

`ssh-keygen -t rsa -f generic-keypair`


### 2.7 Criar usuário programático na AWS

Criar credenciais de acesso para que o Terraform consiga se comunicar com a AWS;

- Acessar o console da AWS 
  - Procurar pelo serviço IAM 
    - Criar um **usuário programático** (douglas.junqueira@xpto.com.br);
      - Adicionar **policy Administrator Access**;

### 2.8 Configurando credencias de acesso 

Para seu código Terraform obter acesso de leitura e escrita na nuvem AWS é necessário fornecer as credencias de acesso.

**Opção 1**

```zsh
$ export AWS_ACCESS_KEY_ID=DAFDAFADSFDFADS
$ export AWS_SECRET_ACCESS_KEY=XDAFAFDFAFAFFSDFD
```

**Opção 2**

```zsh
$ aws configure --profile aws-xpto-dev
```

**Opção 3**

```zsh
$ export AWS_PROFILE=aws-xpto-dev
```

**Opção 4** - *Falha de segurança grave, não façam isso!*

```zsh
provider "aws" {
  region     = "us-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
```

**Opção 5**


```zsh
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}
```

**Opção 6**

```zsh
provider "aws" {
  region  = sa-east-1
  profile = aws-xpto-dev
}
```

### 2.9 Terraform utilizando Docker

Executando o Terraform via Docker.

```zsh
➜  terraform-guide git:(main) ✗  docker container run -ti --name terraform -v $(pwd):/mnt/terraform --entrypoint /bin/sh hashicorp/terraform
```

## 3. Guia Básico

### 3.1 Documentação

Vamos passar dicas de como é estruturada a documentação do Terraform. A linguagem HCL é utilizada para desenver os códigos Terraform. 

Pesquise no Google sobre Terraform language para ter acesso a tudo sobre o Terraform. 

- [Terraform_Language] https://developer.hashicorp.com/terraform/language


A lista de providers que o Terraform consegue trabalhar. 

- [Terraform_Providers] https://registry.terraform.io/browse/providers


Módulos são conjuntos de código do Terraform que são usados para uma determinada finalidade. Existem módulos oficias e módulos criados pela comunidade. 

- [Terraform_Modules] https://registry.terraform.io/browse/modules


### 3.2 Estrutura de arquivos e diretórios 

Arquivos do Terraform são arquivos (.tf). Dentro do mesmo diretório podemos ter diversos arquivos (.tf). Para o Terraform arquivos .tf fazem parte do mesmo código, desde que eles estejam dentro do mesmo diretório. 

Você pode definir o nome que bem entender para criar seus arquivos. É interessante espalhar seus código entre diversos arquivos de forma que facilite a manutenção do código. 

Exemplo de estrutura de arquivos criados para um determinado projeto.

    - 00-variables.tf
    - 01-main.tf
    - 02-outputs.tf
    - 03-locals.tf
    - 04-aws-network.tf
    - 05-azure-network.tf
    - .terraform                 # Diretório que armazena as configurações dos módulos remotos que são baixados. 
    - .terraform.lock.hcl        # Controla as versões dos providers que você está utilizando, é criado pelo Terrafomr, pois se trata de um arquivo de controle interno
    - terraform.tfstate          # Este arquivo é criado após executar o comando do Terraform pelo menos uma vez.  
    - terraform.tfstate.backup   # Armazena o estado anterior da sua infraestrutura
    - terraform.tfvars           # Não é obrigatório ; pode ser utilizado para declarar os valores das variáveis definido no arquivo de variáveis


Exemplo de um arquivo de nome **terraform.tfvars**. O nome "terraform" pode ser substituido por qualquer outro nome, desde que o sufixo seja **(.tfvars)**. 

```vim
$ vim terraform.tfvars
  location              = "West Europe" 
  bucket_name           = "pje-data01"
  storage_account_name  = "dionatas.queiroz"
```

Exemplo do arquivo variables.tf. Definimos as variáveis sem os respectivos valores

```terraform
variable "resource_group_name" {
    description = "Nome para o resource group que será usado"
    type = string
}

variable "location" {
    description = "Localização do resource group"
    type = string
}


variable "" {
    description = "Nome para o storage account"
    type = string
}
```

### 3.3 Estrutura de blocos da linguagem HCL 

No terraform temos 8 tipos diferentes de blocos que nós podemos utilizar em nosso código. 

- [ ] Terraform
- [ ] Providers
- [ ] Resource
- [ ] Data
- [ ] Module
- [ ] Variable
- [ ] Output 
- [ ] Locals

**Seguem os exemplos de blocos**.

```terraform

$ vim main.tf

# Inserir informações do Terraform 
terraform {
  
}

# Definir provider que será utilizado 
provider "aws" {
  
}

# Criar bucket 
resource "aws_s3_bucket" "pje-data01" {
  
}

# Criar insntância na AWS 
resource "aws_instance" "vm01" {
  
}

# Utilizado para pegar alguma informação de fora e trazer para ser utilizada dentro do Terraform
# Neste exemplo vamos pegar a imagem utilizada por uma instancia 
data "aws_ami" "image" {
  
}

# 
module "create_bucket" {
  
}

# Set var
variable "vm_name" {
  
}

# Set output 
output "instance_ip" {
  
}

# Definir funções que utilizamos com frequência em nosso código
locals {
  
}
```

### 3.4 Configuração do bloco Terraform 

- [ x ] **Bloco Terraform**

    - Configurações possíveis 
        - required_version
        - required_provider
        - backend 
        - cloud                 # Configurar o terraform cloud da própria Hashcorp
        - experiments           # Habilitar feature de teste do Terraform antes mesmo de ser liberada 
        - provider_meta         # 

 
- [ list_providers ] https://registry.terraform.io/browse/providers

Segue abaixo exemplo da configuração do Bloco Terraform. Crie um arquivo de nome main.tf e adicione os atributos do Bloco Terraform. 

```zsh
➜  terraform-guide git:(main) vim main.tf

```

```terraform

terraform {

  required_version = "~> 1.6.4"  # Vai funcionar da versão 1.0.0 até a versão 1.0.N
  #required_version = "!= 1.0.0"  # Roda em qualquer versão exceto a versão 1.0.0
  #required_version = ">= 1.0.0"  # Maior que a versão 1.0
  #required_version = ">= 1.0.0, < 1.3.0" # Maior que 1.0.0 e menor que 1.3.0
  #required_version = "1.6.4" # Set a versão do Terraform que foi homologado esse código

  required_providers {

    aws = {
      #version = "~> 5.0"
      #source  = "hashicorp/aws"
      version = ">= 5.30.0"
      source = "hashicorp/terraform-provider-aws"
    }

    azurerm = {
      version = "~> 3.84.0"
      source = "hashicorp/azurerm"

    }

    google = {
        version = "~> 5.8.0"
        source = "hashicorp/google"
    }

  }

  # Definir qual tipo de backend será usado: s3, local, remote, azurerm, consul, gcs, kubernetes e etc...
  backend "s3" {
    bucket = "mybucket"
    key    = "path/to/my/key"
    region = "us-east-1"
  }

}

```

### 3.5 Terraform CLI 

Permite interagir com os providers via binário do Terraform. Ao executar o comando $ terraform você terá acesso aos principais comandos: 

- [x] terraform init       # prepara seu diretório de trabalho para os demais comandos
- [x] terraform validate   # Verifica se a configuração é valida 
- [x] terraform plan       # Exibe mudanças requeridas pela configuração atual 
- [x] terraform apply      # Cria ou atualiza a infraestrutura
- [x] terraform destroy    # Destroy a infra criada previamente 

### 3.6 Credências de acesso

- [Hard-coded credentials are not recommended] https://registry.terraform.io/providers/hashicorp/aws/latest/docs

- [X] **Warning**

Credenciais codificadas não são recomendadas em nenhuma configuração do Terraform e correm o risco de vazamento de segredo caso este arquivo seja enviado a um sistema de controle de versão público.

```terraform
provider "aws" {
  region     = "us-west-2"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
```

**Environment Variables**

```
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
$ export AWS_REGION="us-west-2"
$ terraform plan
```

**Obs**: Cada provider tem uma forma de autenticação diferente. 


### 3.7 Criação de Bucket na AWS 

Para o Terraform não importa se código que você vai aplicar está em um arquivo só ou está dividido em vários arquivos dentro do memso diretório.

As versões dos providers são atualizadas com frequência, entretando fiquem atentos. 

Existem alguns argumentos que se forem alterados o Terraform vai remover e recriar novamente o recurso, como por exemplo:

- [s3_resource] https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#argument-reference

  - bucket 
  - object_lock_enabled 
  - bucket_prefix

Mandatório a utilização to **"terraform init"** no início do projeto, com isso o comando vai baixar os módulos remotos e providers.

Ao executar o **"terraform init"** será criado dentro do diretório atual um diretório oculto de nome **".terraform"** com todas as bibliotecas dos providers declarados no código construido. 

Outro arquivo criado é o **".terraform.lock.hcl"** onde armazena as informações dos providers utilizados. É baseado nesse arquivo que o Terraform verifica se o provider foi modificado ou não em relação a configuração inicial. 



```zsh
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 init
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 fmt -check  # Informa quais os arquivos ele faria uma mudança se o comando fosse executado
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 fmt -diff  # Formata os arquivos e exibe o que foi alterado.  
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 validate
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 providers
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 plan -out plan.out
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 show plan.out   # Exibe o plano que foi armazenado no comando "terraform plan -out plan.out"
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 apply plan.out -auto-approve
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 plan -out plan.out -destroy 
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 apply plan.out  # Vai remover os recursos do plano criado com o comando anterior. 
➜  dionatas-labs git:(master) ✗ terraform -chdir=projeto_03 destroy 
```


## 4. Variáveis

O Terraform segue a ordem de precedência para definir os valores das variáveis conforme documentação.  

- [ variables ] https://developer.hashicorp.com/terraform/language/values/variables

- [ ] terraform.tfvars file 
- [ ] terraform.tfvars.json file 
- [ ] *.auto.tfvars or *.auto.tfvars.json files
- [ ] Any -var and -var-file options on the command line.
- [ ] export TF_VAR_aws_region=sa-east-1

### Alimentando variáveis de diferentes formas 

Seguem os possíveis argumentos para declaração das variáveis.

```zsh
variable "teste" {
    description = "value"  #É recomendado que forneça uma descrição para a variável. 
    type = string
    default = "12"         #Valor default que a variável vai assumir

    # validation {
      
    # }
    
    sensitive = false
    nullable = false
}

variable "aws_region" {
    description = "Configure the region that will be used in AWS."
    type = string
  
}

variable "aws_profile" {
    description = "Configure the profile that will be used in AWS."
    type = string
  
}
```

Utilizando as variáveis definidas no bloco acima. 

```zsh
# Definir provider que será utilizado 
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile 

  # Essas tags serão aplicadas em todos os recursos que serão criados
  default_tags {
    tags = {
      owner     = "dionatas.queiroz@hepta.com.br"
      managedBy = "Terraform"
    }
  }
}
```

Passando variáveis via command line. 

```zsh
$ terraform apply -var="image_id=ami-abc123"

$ terraform apply -var='image_id_list=["ami-abc123","ami-def456"]' -var="instance_type=t2.micro"

$ terraform apply -var='image_id_map={"us-east-1":"ami-abc123","us-east-2":"ami-def456"}'

$ terraform plan -out plan.out -var aws_region="us-east-1"

$ terraform apply plan.out
```

Uma outra forma de declarar uma variável. 

```zsh
export TF_VAR_aws_region=us-east-1
unset TF_VAR_aws_region
```

Utilizando o arquivo terraform.tfvars para definir as variáveis

```zsh
touch terraform.tfvars

aws_region = "sa-east-1"
aws_profile = "aws-tjba-dev"
```

Podemos utilizar arquivos com o sufixo auto.tfvars conforme exemplo abaixo:

```zsh
touch dev.auto.tfvars prod.auto.tfvars simulacao.auto.tfvars

aws_region = "sa-east-1"
aws_profile = "aws-tjba-dev"

$ terraform plan -out plan.out -out plan.out -var aws_region="sa-east-1"
```

É possível utilizar um arquivo qualquer para declarar as variáveis desde quando use a opção -var-file=xpto

```zsh
touch develop-vars
  
  aws_region = "sa-east-1"
  aws_profile = "aws-tjba-dev"

$ terraform plan -var-file=develop-vars

```

Declarando as variáveis do tipo (map). Em sua declaração contém chaves e valores. 

```zsh
variable "amis_sao_paulo" {
  description = "Set AMIS from sa-east-1"
  type = map(string)

  default = {
    "amazon-linux" = "ami-0f4e579ad17e32ab7"    # Amazon Linux 2023
    "ubuntu" = "ami-0b6c2d49148000cd5"          # Ubuntu Server 22.04 LTS
    "windows" = "ami-0c3459430f11fd691"         # Windows Server 2022
    "redhat"  = "ami-00b45eebb277341fe"         # Red Hat Enterprise Linux 9
    "suse-linux" = "ami-06fb9e1a37c127097"      # SUSE Linux Enterprise 15 SP5
    "debian"  = "ami-0c0746ac7168488ae"         # Debian 12
    
  }
}
```

Utilizando variável do tipo **map** no atributo "ami".

```zsh
# AWS_INSTANCE
resource "aws_instance" "lab-dependency_ohio" {
  provider      = aws.ohio_region
  ami           = var.amis_ohio["amazon-linux"]
  instance_type = "t3a.small"
  key_name      = "multicloud-key"

  subnet_id = "subnet-01d9df434b66e8ecb"

  #vpc_security_group_ids = ["sg-021190ef7a86f9320", "sg-063ddae79da86bf91", "sg-074d7238974a31647"]  
  vpc_security_group_ids = [aws_security_group.allow_ssh_ohio.id, aws_security_group.allow_tls_ohio.id, aws_security_group.allow_services_default_ohio.id]

  root_block_device {
    volume_type = "gp3"
    volume_size = 30
  }

  count = 1

  tags = {
    Name = "lab-dependency_ohio-${count.index}"
  }
}
```

É possível sobrescrever os valores das variáveis declaradas nos arquivos .tf

**TF_VAR_NAME**

```zsh
$ export TF_VAR_region=as-east-1
$ export TF_VAR_aws_region=as-east-1
$ export TF_VAR_ami=ami-240981049810
$ export TF_VAR_alist='[1,2,3]'
$ export TF_VAR_amap='{ foo = "bar", baz = "qux" }'

$ export TF_VAR_instance_type="t3.medium"
```

```zsh
$ terraform plan -var aws_region="sa-east-1" -var environment="dev"
```

 
É possível definir as variáveis criando o arquivo de nome **dev.tfvars** e 

```zsh
$ touch dev.tfvars
 
environments = "dev"
aws_region = "as-east-1"
instance_type = "t3.micro"
 
$ terraform plan -var-file="dev.tfvars"

```

$ export AWS_ACCESS_KEY_ID="xxxxxxxxxx"
$ export AWS_SECRET_ACCESS_KEY="xxxxxxxxxxxx"


## 5. Referenciando atributos de outros blocos

É possível reutilizar um atributo já declarado conforme o exemplo abaixo. 

Observe que o atributo de nome **vpc_security_group_ids** da VM **lab** está utilizando como referência o nome do security group já criado.

Podemos referenciar qualquer argumento de um recurso.

```zsh
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "vpc-0b555b5d30ffd76f4"

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = ["168.228.243.22/32", "177.134.182.235/32"] # Funciona como uma lista
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_security_group" "allow_services_default" {
  name        = "allow_services_default"
  description = "Allow default inbound traffic"

  vpc_id = "vpc-0b555b5d30ffd76f4"

  ingress {
    description = "Allow ICMP"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = ["168.228.243.22/32", "177.134.182.235/32"]   # Works like a list
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = ["168.228.243.22/32", "177.134.182.235/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_services_default"
  }
}
}

resource "aws_instance" "lab" {
  ami           = "ami-0b6c2d49148000cd5"
  instance_type = "t3a.small"
  key_name      = "multicloud-key"

  subnet_id = "subnet-01bd65c9b5cc6afa2"

  #vpc_security_group_ids = ["sg-021190ef7a86f9320", "sg-063ddae79da86bf91", "sg-074d7238974a31647"]  
  vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.allow_tls.id, aws_security_group.allow_services_default.id]

  root_block_device {
     volume_type = "gp3"
     volume_size = 30
  }

  count = 1

  tags = {
    Name = "lab-${count.index}"
  }

}

```


## 6. Local Values

O **Bloco Locals** permite dar um nome para uma função que se repete com frequência em nosso código, com isso não é necessário declarar várias vezes a mesma função. 


A **Cloud Azure** não permite que sejam adicionadas **defaults_tags** dentro do bloco provider, por isso é necessário adicionar um **Locals** desse tipo.

```zsh

$ touch locals.tf

# Declarando Locals
locals {
  common_tags = {
    Project = "pje1g"
    Company = "hepta"
  }
}

# Acessando o Locals definido acima
resource "aws_s3_bucket" "lab-pje1g-data01" {
  bucket = "lab-pje1g-data01"

  tags = local.common_tags
}
```

## 7. Outputs

Serve para nós pegarmos algumas informações que produzidas pelo código do Terraform. 

Exemplo de como usar output para exibir informação do bucket. 

```zsh
resource "aws_s3_bucket" "lab-data01" {
  bucket = "lab-pje1g-data01"

  tags = local.common_tags
}

output "bucket_pje" {
  description = "Bucket name from PJe project."
  value = aws_s3_bucket.lab-data01.bucket
}

output "bastion01-private_ip" {
  description = "Private IP from VM bastion01"
  value = aws_instance.bastion-connect01.private_ip
  sensitive = false
}
```



### 7.1 Utilizando os Outputs 

Exibe os outputs que estejam configurados no código. 

```zsh
output "bucket_pje" {
  description = "Bucket name from PJe project."
  value       = aws_s3_bucket.lab-pje1g-data01.bucket
}

output "data_01_region" {
  description = "value"
  value = aws_s3_bucket.lab-pje1g-data01.region
  sensitive = true
}

output "aws_region" {
    value = var.aws_region
  
}
```

```zsh

$ terraform output
$ terraform output bucket_pje
$ terraform output -raw bucket_pje
$ terraform output -raw data_01_region # Mesmo com o atributo "sensitive" habilitado ele vai mostar o valor da variável. 
```


Definindo outputs 

```
output "lab-bastion" {
    value = aws_instance.lab-bastioin.private_ip
  
}
```

Checando os outputs configurados

```
$ terraform output -json
$ terraform output -raw
```
 
### Armazenando o estado desejado do Terraform

```zsh
$ terraform plan -out="tfplan.out"
$ terraform apply "tfplan.out" # Roda apenas uma vez. É preciso regenerar o arquivo cada vez que for executar.
```


## 8. Criar Resources

### 8.1 Criar Security Group

Se um Security Group não for especificado, será aplicado um Security Group default, assim como é na maioria dos atributos, possuem valores defaults.

Modelo de Security Group liberando a porta 22 passando no atributo "cidr_blocks" os IPs permitidos.

```zsh
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "vpc-0b555b5d30ffd76f4"

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["168.228.243.22/32", "177.134.182.235/32"] # Funciona como uma lista
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}
```

Uma vez criado o Security Group é necessário associar a instância EC2 desejada passando o atributo **vpc_security_group_ids**. 

```zsh
resource "aws_instance" "lab" {
  ami           = "ami-0b6c2d49148000cd5"
  instance_type = "t3a.small"
  key_name      = "multicloud-key"

  subnet_id = "subnet-01bd65c9b5cc6afa2"

  vpc_security_group_ids = ["sg-021190ef7a86f9320", "sg-063ddae79da86bf91"]  

  count = 1

  tags = {
    Name = "lab-${count.index}"
  }

}
```

O comando **terraform show** lê o arquivo **terraform.tfstate** e exibe as informações associadas (IP, rede, etc).



### 8.2 Configurando VPC

É necessário conhecer o seu provider, seja ele AWS, Google, Azure e etc...



### 8.3 Configurando keypair 

```zsh
resource "aws_key_pair" "multicloud-key" {
  provider = aws.california_region
  key_name   = "multicloud-key"
  public_key = file("./multicloud-key.pub")
  #public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgij6ee7wl45Z2cemR12YuGD+Jm3W5p5rboExdhWG2r0jYWqTWEonpF80CEGto/bcI/xgLegylPsXd5eEo6AcXWNqbyKjqvzU1A8NwFif4vBt5XBiREMUBUZF6yscEj5QcBoU+8eSOwasEy0uIygxuYeTY41hQp+LEkrLxMkUUCeMtl3UYZpbLwC/pP1hafxN5cYF8PAFIDtFQbAIQc0qcHShdtu6IkMK0zGDaIuo5g/iaK58ifc1w6kyvx/S/iXy9yjQOV4AS15Hgf1oR5cd0DYBz7D9Mlho9AY+t5K019f4hdvUtJZTC2W22ZlPEKbNe3a50lgLs8raTAQ3tU9dN dionatas@DESKTOP-CF783SL"
}
```


## 9. Configurando dependência entre os recursos 

Neste caso a instância só será criada depois que o bucket for criado, conforme terraform apply.

```zsh
# AWS_INSTANCE
resource "aws_instance" "lab-dependency" {
  ami           = "ami-0b6c2d49148000cd5"
  instance_type = "t3a.small"
  key_name      = "multicloud-key"

  subnet_id = "subnet-01bd65c9b5cc6afa2"

  #vpc_security_group_ids = ["sg-021190ef7a86f9320", "sg-063ddae79da86bf91", "sg-074d7238974a31647"]  
  vpc_security_group_ids = [aws_security_group.allow_ssh.id, aws_security_group.allow_tls.id, aws_security_group.allow_services_default.id]

  root_block_device {
     volume_type = "gp3"
     volume_size = 30
  }

  count = 1

  tags = {
    Name = "lab-dependency-${count.index}"
  }

  depends_on = [ aws_s3_bucket.lab-dependency-beteween-resources ]

}

# AWS_S3_BUCKET 
resource "aws_s3_bucket" "lab-dependency-beteween-resources" {
  bucket = "lab-dependency-beteween-resources"

  tags = local.common_tags
}
```

## 10. Novos recursos em diferentes regiões 

Quando precisamos subir vários recursos em regiões diferentes. 

```zsh
# Provider
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile

  # Essas tags serão aplicadas em todos os recursos que serão criados
  default_tags {
    tags = {
      Owner       = "dionatas.queiroz@hepta.com.br"
      ManagedBy   = "Terraform"
      Terraform   = "true"
      Environment = "develop"
    }
  }
}

provider "aws" {
  alias   = "ohio_region"
  region  = "us-east-2"
  profile = var.aws_profile

  default_tags {
    tags = {
      Owner       = "dionatas.queiroz@hepta.com.br"
      ManagedBy   = "Terraform"
      Terraform   = "true"
      Environment = "develop"
    }
  }
}

# Securiy Group 
resource "aws_security_group" "allow_tls_ohio" {
  provider    = aws.ohio_region
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-0177f6c4d08684cd3"

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = ["168.228.243.22/32", "177.134.182.235/32"]
    #ipv6_cidr_blocks = []
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls_ohio"
  }
}
```


## 11. Substituindo um recurso com o comando abaixo
 
```zsh
$ terraform apply -replace="aws_instance.example[0]"]
``````


### Removendo Recursos

Existem algumas formas de remover os recursos via Terraform. 

1) Usando o comando "terraform destroy"

```zsh
➜  projeto_03 git:(master) ✗ terraform state list
➜  projeto_03 git:(master) ✗ terraform destroy -target aws_dynamodb_table.dynamodb-table-lab
➜  projeto_03 git:(master) ✗ terraform destroy -target aws_instance.bastion
```

2) Comentando e salvando o arquivo do recurso.

3) Removendo o arquivo com o recurso em questão.

 

## 12. Shell Tab-completion

```zsh
$ terraform -install-autocomplete
$ terraform -uninstall-autocomplete
```

## 13. Logs

Nível de verbosidade
- trace
- debug
- info
- warn
- error
 
```zsh
$ export TF_LOG=trace
$ export TF_LOG=off
```


Para persistir os logs na máquina local.

```zsh
$ export TF_LOG_PATH=./terraform.log
```

## 14. Comandos Terraform

### 14.1 Terraform console 

É possível recuperar várias informações do ambiente via terraform console. 

```zsh 
$ terraform console # Para sair digite exit

> var.aws_region
> var.aws_profile
> 
```
### 14.2 Terraform validate and fmt 

```zsh
$ terraform validate -json

$ terraform fmt  # formata o código no padrão
```

### 14.3 Terraform state / terraform show

O Terraform utiliza os dados de estado para lembrar quais dados do mundo real corresponde com os recursos configurados.

É graças a esse recurso que o Terraform se torna **idempotente**, ou seja, podemos aplicar o mesmo código diversas vezes sem que o resultado se altere. Ele tem a capacidade de verificar que o recurso já existe e não cria novamente. 

O arquivo **terraform.tfstate** precisa ser armazenado em um local seguro, pois contém todas informações da infra provisionada. 

O arquivo **terraform.tfstate.backup** armazena uma versão anterior do arquivo **terraform.tfstate** por isso funciona como um backup das configurações apliacadas. 

Ao executar determinados comnandos é gerado de forma temporária um arquivo de nome **.terraform.state.lock.info** que exibe informações de quem executou o comando de mudança de estado. 


```zsh 
iac-tjba-dev git:(master) ✗ terraform -chdir=storage state list

iac-tjba-dev git:(master) ✗ terraform -chdir=storage state list aws_s3_bucket.bucket-tjba-01
 
iac-tjba-dev git:(master) ✗ terraform -chdir=storage state list -id=$(terraform -chdir=storage output -json instance | jq -r '.id')
 
iac-tjba-dev git:(master) ✗ terraform -chdir=storage state show aws_s3_bucket.bucket-tjba-01
``` 

É possível renomear um recurso dentro do state para que o Terraform não recrie o recurso utilizando o parâmetro state move

Para que o recurso (por exemplo um bucket ou uma instancia) não seja removido renomei o state do Terraform


```zsh
$ mkdir storage
$ touch main.tf
$ touch bucket.tf
$ touch variable.tf


terraform -chdir=storage init

terraform -chdir=storage plan -out plan.out

terraform -chdir=storage apply plan.out

terraform -chdir=storage show plan.out

terraform -chdir=storage state list 

# Faz o download(backup) do state remoto 
terraform -chdir=storage state pull > storage/state.tfstate.manual

terraform -chdir=storage state mv aws_s3_bucket.lab-tjba-data030 aws_s3_bucket.lab-tjba-data030-new


terraform -chdir=storage state list

terraform -chdir=storage state push -force storage/state.tfstate.manual

terraform -chdir=storage providers 



resource "aws_s3_bucket" "lab-tjba-data010" {
  bucket = "lab-tjba-data010"
}

resource "aws_s3_bucket" "lab-tjba-data020" {
  bucket = "lab-tjba-data020"
}

resource "aws_s3_bucket" "lab-tjba-data030" {
  bucket = "lab-tjba-data030"
}

```

### 14.4 Terraform import | terraform refresh

Para codificar um recurso que já existe na cloud, é necessário primeiro adicionar o recurso em um arquivo (.tf) e depois executar o import do recurso, feito isso o terraform não irá tentar adicionar o recurso novamente. 

Adicionando o bloco de código que será importado pelo Terraform. Até um momento não existe um comando que faça o import e adicione o bloco do recurso no arquivo (.tf).

```zsh
resource "aws_s3_bucket" "lab-tjba-data010" {
  bucket = "lab-tjba-data010"
}
```

Executando o comando terraform import

```zsh
$ terraform import aws_s3_bucket.lab-tjba-data010 lab-tjba-data010
```

O comando **terraform refresh** é utilizado para atualizar o state do terraform, contudo não é muito utilizado pois o comando terraform plan  e terraform apply já fazem isso. 

0bs: Se for adicionado uma tag em um bucket e executado o comando **terraform refresh** , ao executar o **terraform state show aws_s3_bucket.lab-tjba-data010** será possível ver a tag associada ao bucket ou qualquer outro recurso. 

Entretando, ao executar o comando terraform plan, será informado sobre a remoção da tag, com isso é necessário adicionar manualmente a tag no código. 

Comando não muito utilizado, atualiza o state do Terraform

```zsh
$ terraform refresh
```

### 14.5 Comando terraform init -reconfigure | -migrate-state | -backend-config

Caso você altere algum atributo do backend, é possível utilizar o comando abaixo. 

```zsh
$ terraform init -reconfigure
```

É possível migrar o state do terraform, basta alterar a key do backend e rodar o comando abaixo

```zsh

# From
backend "s3" {
    bucket         = "lab-tfstate-tjba"
    key            = "lab/storage/terraform.tfstate"
    region         = "sa-east-1"
    profile        = "aws-tjba-dev"
    dynamodb_table = "lab-terraform-tfstate"
  }

# To
backend "s3" {
    bucket         = "lab-tfstate-tjba"
    key            = "dev/compute/terraform.tfstate"
    region         = "sa-east-1"
    profile        = "aws-tjba-dev"
    dynamodb_table = "lab-terraform-tfstate"
  }

$ terraform init -migrate-state   # O comando vai migrar o state da key /lab/storage para dev/compute
$ terraform init -force-copy      # Faz o migrate sem pedir confirmação
```

### 14.6 Passando o argumento -backend-config

O terraform não ler variáveis dentro do bloco do Terraform. Configurando os states como exemplo abaixo eles não serão sobrescritos.

A estrutura abaixo é muito utilizada em pipelines.

Obs: Os arquivos e diretórios **.terraform**, **terraform.state**, **.terraform.lock.hcl** devem ser configurados no arquivo .gitignore

```zsh

$ touch main.tf

# O bloco backend deve ficar vazio dentro da config do terraform
backend "s3" {
    
  }

$ touch backend-dev.hcl

bucket         = "lab-tfstate-tjba"
key            = "dev/compute/terraform.tfstate"
region         = "sa-east-1"
profile        = "aws-tjba-dev"
dynamodb_table = "lab-terraform-tfstate"


$ touch backend-prod.hcl

bucket         = "lab-tfstate-tjba"
key            = "prod/compute/terraform.tfstate"
region         = "sa-east-1"
profile        = "aws-tjba-dev"
dynamodb_table = "lab-terraform-tfstate"

$ terraform init -backend-config=backend-prod.hcl
```

### 14.7 Terraform force-unlock 

Para remover o lock execute o comando abaixo passando o ID informado pelo comando plan ou apply. 

```zsh
$ terraform -chdir=storage force-unlock c669fa48-755e-9c87-7923-aa7d0fbcf14d
```


### 14.8 Comando terraform get

```zsh
# Add o módulo dentro do arquivo main.tf 
module "iam" {
  source  = "terraform-aws-modules/iam/aws"
  version = "5.33.0"
}

# Execute o comando abaixo para baixar o módulo dentro de .terraform/modules
$ terraform get
```


### 14.9 Comando terraform -target / -replace

O comando **terraform -replace** pode ser utilizado para substituir uma instância.

```zsh
$ terraform plan -target aws_key_pair.multicloud-key -out plan.out

terraform -chdir=compute plan -replace aws_key_pair.multicloud-key -out plan.out
```


## 15. Trabalhando em Equipes 

https://developer.hashicorp.com/terraform/language/settings/backends/remote

### 15.1 Terraform Cloud

### 15.2 Remote State using backend S3

- [backends] https://developer.hashicorp.com/terraform/language/settings/backends/s3

Se não for especificado nenhum **Remote State** o Terraform vai salvar o arquivo localmente.

O Remote State permite que várias pessoas de locais diferentes utilizem possam utilizar o mesmo código. 

Ele salva o arquivo terraform.tfstate em um local remoto possibilitando que multiplos usuários utilizem o código para provisionar novos recursos ou atualizar os recursos existentes. 

1) Create bucket

```zsh
#Bucket utilizado pelo terraform-tfstate - Remote State backend S3
resource "aws_s3_bucket" "lab-tfstate-tjba" {
  bucket = "lab-tfstate-tjba"
}

resource "aws_s3_bucket_versioning" "versioning-lab-tfstate" {
  bucket = aws_s3_bucket.lab-tfstate-tjba.id
  versioning_configuration {
    status = "Enabled"
  }
}
```

2) Create table in DynamoDB

```zsh
resource "aws_dynamodb_table" "lab-terraform-tfstate" {
   name = "lab-terraform-tfstate"
   hash_key = "LockID"
   read_capacity = 20
   write_capacity = 20

   attribute {
      name = "LockID"
      type = "S"
   }

   tags = {
     Name = "Terraform Lock Table"
   }
}
```

3) Set backend S3 in Terraform block 

```zsh
terraform {

  required_version = ">= 1.6.4" # Set a versão do Terraform que foi homologado esse código

  required_providers {

    aws = {
      version = ">= 5.30.0"
      source  = "hashicorp/aws"
    }
  }

  backend "s3" {
    bucket = "lab-tfstate-tjba"
    key    = "lab/terraform.tfstate"
    region = "sa-east-1"
    profile = "aws-tjba-dev"
    dynamodb_table = "lab-terraform-tfstate"
  }
}
```


## 16. Provisioners

- [provisioners] https://developer.hashicorp.com/terraform/language/resources/provisioners/syntax

Solução alternativa para executar tarefas na infraestrutura provisionada. Quando o provisioners é utilizado o terraform não consegue fazer um controle ideal dos recursos. 

Ao invés de utilizar provisioners, é recomendado configuar o argumento de cada providers para executar determinadas tarefas na instância alvo

Por exemplo, na *AWS* é possível utilizar o argumento **user_data**, na *Azure* **custom_data** e na *Google cloud* use **metadata**

## 17. Módulos 

### 17.1 Módulos Locais

Quando utilizamos módulos os parâmetros só podem ser acessados ser forem criados outputs. Portanto, sempre faça o output daquilo que você vai precisar usar em seu Root Modules. 

- Módulos Locais
- Módulos remotos 

- [Root Module]
- [Child Modules]


Root Module 
  - Network
  - Storage


```zsh
# Insira no final do arquivo main.tf
module "network" {
  source = "./network"

  cidr_vpc    = "192.168.0.0/16"
  cidr_subnet = "192.168.10.0/24"
  environment = "lab"
}
```

```zsh
$ terraform fmt -recursive
$ terraform validate
```

### 17.2 Módulos Remotos



## 18. Recursos Avançado

### 18.1 Workspaces

O workstate padrão é chamado de default. O terraform vai criar um diretório de nome terraform.tfstate.d para armazer o state de cada workpspace criado. 

Workspace suporta diversos tipos de backends, como, local, s3 e etc...

```zsh
$ terrraform workspace list
$ terraform workspace new dev # É criado um arquivo de nome environment dentro do diretório .terraform
$ terraform workspace new prod
$ terraform workspace select dev 
```

Após criar um workspace será criado dentro do bucket que armazena o state um diretório de nome **env** e dentro dele vai o nome do workspace criado. 


### 18.2 Data Source

É uma funcionalidade do terraform que nos permite trazer alguns informações que foram geradas fora do código do terraform para dentro do código. 



### 18.4 Resource Time Sleep 

Configura um tempo de espera entre a criação de recursos. 


### 18.5 Terrafore em Pipelines



### 18.6 Meta Argumento Lifecycle

É possível definir padrão para o ciclo de vida dos recursos. Existem alguns argumentos disponíveis:

1) create_before_destroy
2) prevent_destroy
3) ignore_changes
4) replace_triggered_by

Segue exemplo: 

```zsh
esource "aws_s3_bucket" "lab-tjba-data010" {
  bucket = "lab-tjba-data010"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [ 
      tags
     ]
  }
}
```

Configurando argumento "prevent_destroy = true" dentro do bloco do recurso desejado não será possível remover o recurso ao executar o comando "terraform destroy".

Já o argumento "ignore_changes" instrui ao terraform desconsiderar mudanças que houverem na configuração de tags.
